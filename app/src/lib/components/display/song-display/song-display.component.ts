import { Component, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';

import { Song, SongService } from '@snaps/api';

import { DisplaysEntity } from '../displays-entity';


@Component({
  selector: 'app-song-display',
  templateUrl: './song-display.component.html',
  styleUrls: ['./song-display.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SongDisplayComponent extends DisplaysEntity<Song> {
  constructor(protected songService: SongService) {
    super(songService);
  }
}
