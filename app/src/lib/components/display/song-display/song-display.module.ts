import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaterialSharedModule } from '@snaps/core';

import { SongDisplayComponent } from './song-display.component';


@NgModule({
  declarations: [
    SongDisplayComponent
  ],
  exports: [
    SongDisplayComponent
  ],
  imports: [
    CommonModule,
    MaterialSharedModule
  ]
})
export class SongDisplayModule { }
