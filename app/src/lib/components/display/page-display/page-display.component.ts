import { Component, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';

import { EntityType, Page, PageService } from '@snaps/api';
import { pluck } from 'rxjs/operators';

import { DisplaysEntity } from '../displays-entity';


@Component({
  selector: 'app-page-display',
  templateUrl: './page-display.component.html',
  styleUrls: ['./page-display.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PageDisplayComponent extends DisplaysEntity<Page> {

  entities$ = this.entity$.pipe(
    pluck('entities')
  );

  songType: EntityType = 'song';

  constructor(protected pageService: PageService) {
    super(pageService);
  }
}
