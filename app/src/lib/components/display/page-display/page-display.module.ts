import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageDisplayComponent } from './page-display.component';



@NgModule({
  declarations: [
    PageDisplayComponent
  ],
  exports: [
    PageDisplayComponent
  ],
  imports: [
    CommonModule
  ]
})
export class PageDisplayModule { }
