import { Directive, Input } from '@angular/core'
import { Observable, race, ReplaySubject, combineLatest, BehaviorSubject, asapScheduler } from 'rxjs';
import { switchMap, map, debounceTime } from 'rxjs/operators';

import { CrudApi, Entity } from '@snaps/api';

import { CanLeak } from '../mixins/can-leak';


@Directive({
  host: {
    '[class.c-pointer]': 'hasRouterLink'
  }
})
export class DisplaysEntity<T extends Entity> extends CanLeak {

  @Input()
  set routerLink(value: string) {
    this.hasRouterLink = !!value;
  }
  hasRouterLink: boolean;

  @Input()
  set id(value: string) {
    if (value) {
      this.idSub.next(value);
    }
  }
  private idSub = new ReplaySubject<string>();

  @Input()
  set entity(value: T) {
    if (value !== null) {
      this.entitySub.next(value);
    }
  }
  private entitySub = new ReplaySubject<T>();

  @Input()
  get small(): boolean {
    return this.smallSub.value;
  }
  set small(value: boolean) {
    this.smallSub.next(value);
  }
  private smallSub = new BehaviorSubject<boolean>(false);

  entity$: Observable<T> = race(
    this.entitySub.asObservable(),
    this.idSub.pipe(switchMap(id => this.apiService.get(id)))
  );

  id$: Observable<string> = race(
    this.idSub.asObservable(),
    this.entitySub.pipe(map(entity => entity.id))
  );

  constructor(
    protected apiService: CrudApi<T>
  ) {
    super();
  }
}
