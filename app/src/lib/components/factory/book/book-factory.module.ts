import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookFactoryComponent } from './book-factory.component';



@NgModule({
  declarations: [
    BookFactoryComponent
  ],
  exports: [
    BookFactoryComponent
  ],
  imports: [
    CommonModule
  ]
})
export class BookFactoryModule { }
