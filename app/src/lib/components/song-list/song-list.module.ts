import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import { MaterialSharedModule } from '@snaps/core';

import { SongListComponent } from './song-list.component';


@NgModule({
  declarations: [
    SongListComponent
  ],
  exports: [
    SongListComponent
  ],
  imports: [
    CommonModule,
    MaterialSharedModule,
    ReactiveFormsModule,
    RouterModule
  ]
})
export class SongListModule { }
