import { Component, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';

import { SongService } from '@snaps/api';

import { SongStore } from 'lib/state/song-state';
import { CanLeak } from '../mixins/can-leak';

@Component({
  selector: 'app-song-list',
  templateUrl: './song-list.component.html',
  styleUrls: ['./song-list.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  host: {
    'class': 'app-song-list'
  }
})
export class SongListComponent extends CanLeak {

  songs$ = this.songService.getAll();

  formControl = new FormControl();

  selectedSongs$ = this.formControl.valueChanges;

  constructor(
    private songService: SongService,
    private songStore: SongStore
  ) {
    super();
    this.subscribe();
  }

  private subscribe(): void {
    this.selectedSongs$
      .pipe(takeUntil(this.destroySub))
      .subscribe(songs => this.songStore.set(songs));
  }
}
