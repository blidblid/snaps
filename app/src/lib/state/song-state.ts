import { Injectable } from '@angular/core';

import { StoreBase } from '@snaps/core';
import { Song } from '@snaps/api';


@Injectable({ providedIn: 'root' })
export class SongStore extends StoreBase<Song> {
  constructor() {
    super('song');
  }
}
