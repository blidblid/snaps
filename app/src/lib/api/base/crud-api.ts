import { AngularFirestore } from '@angular/fire/firestore';
import { firestore } from 'firebase/app';
import { Observable, combineLatest, of } from 'rxjs';
import { tap } from 'rxjs/operators';

import { Entity } from './entity-model';


export class CrudApi<T extends Entity> {
  constructor(
    protected afs: AngularFirestore,
    protected path: string
  ) { }

  get(id: string): Observable<T> {
    return this.afs.doc<T>(this.getDocPath(id)).valueChanges();
  }

  getMany(ids: string[]): Observable<T[]> {
    if (ids.length === 0) {
      return of([]);
    }

    return combineLatest(ids.map(id => this.get(id)));
  }

  getAll(): Observable<T[]> {
    return this.afs.collection<T>(this.getCollectionPath()).valueChanges();
  }

  update(id: string, entity: Partial<T>, removeInArrays = false): void {
    this.afs
      .doc<T>(this.getDocPath(id))
      .update(this.withArrayOperation(entity, removeInArrays));
  }

  set(id: string, entity: T): void {
    this.afs
      .doc<T>(this.getDocPath(id))
      .set({ ...entity, id });
  }

  delete(id: string): void {
    this.afs.doc<T>(this.getDocPath(id)).delete();
    this.afs.doc<T>(this.getListingDocPath(id)).delete();
  }

  getGeneratedId(): string {
    return this.afs.createId();
  }

  private getCollectionPath(): string {
    return `crud/entity/${this.path}`;
  }

  private getListingCollectionPath(): string {
    return `crud/listing/${this.path}`;
  }

  private getDocPath(id: string): string {
    return `${this.getCollectionPath()}/${id}`;
  }

  private getListingDocPath(id: string): string {
    return `${this.getListingCollectionPath()}/${id}`;
  }

  // eslint-disable-next-line
  private withArrayOperation(value: object, remove = false): object {
    const unionArrays = Object
      .keys(value)
      .filter(key => Array.isArray(value[key]))
      .reduce((acc, key) => {
        return {
          ...acc,
          [key]: remove ? firestore.FieldValue.arrayRemove(...value[key]) : firestore.FieldValue.arrayUnion(...value[key])
        };
      }, {});

    return { ...value, ...unionArrays };
  }
}
