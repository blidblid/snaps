export interface Entity {
  id: string;
  type?: EntityType;
}

export type EntityType = 'song';
