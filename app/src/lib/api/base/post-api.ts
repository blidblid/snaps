import { AngularFirestore } from '@angular/fire/firestore';


export class PostApi<T> {
  constructor(
    private firestore: AngularFirestore,
    private path: string
  ) { }

  push(value: T): void {
    const id = this.firestore.createId();
    this.firestore.doc<T>(this.getDocPath(id)).set(value);
  }

  private getCollectionPath(): string {
    return `post/${this.path}`
  }

  private getDocPath(id: string): string {
    return `${this.getCollectionPath()}/${id}`
  }
}
