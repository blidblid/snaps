import { Entity } from '../base';


export interface Page extends Entity {
  entities: Entity[];
}
