import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

import { CrudApi } from '../base';
import { Page } from './page-model';


@Injectable({
  providedIn: 'root'
})
export class PageService extends CrudApi<Page> {
  constructor(protected afs: AngularFirestore) {
    super(afs, 'page');
  }
}
