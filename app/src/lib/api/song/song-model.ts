import { Entity } from '../base';

export interface Song extends Entity {
  title: string;
  melody: string;
  verses: Verse[];
}

export interface Verse {
  lines: string[];
  title?: string;
}

