import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

import { CrudApi } from '../base';
import { Song } from './song-model';


@Injectable({
  providedIn: 'root'
})
export class SongService extends CrudApi<Song> {
  constructor(protected afs: AngularFirestore) {
    super(afs, 'song');
  }
}
