import { Entity } from '../base';


export interface Book extends Entity {
  title: string;
  pageIds: string[];
}

export const EMPTY_BOOK: Omit<Book, 'id'> = {
  title: 'A nameless book',
  pageIds: []
}
