import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

import { CrudApi } from '../base';
import { Book, EMPTY_BOOK } from './book-model';


@Injectable({
  providedIn: 'root'
})
export class BookService extends CrudApi<Book> {
  constructor(protected afs: AngularFirestore) {
    super(afs, 'book');
  }

  createEmptyBook(): string {
    const id = this.getGeneratedId();
    this.set(id, { ...EMPTY_BOOK, id });
    return id;
  }
}
