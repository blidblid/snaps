export enum Action {
  Add,
  Remove,
  Clear,
  Set,
  Push,
  Pop,
  Update
}

export interface StoreAction<T> {
  action: Action;
  entity?: T;
  target?: T;
}

