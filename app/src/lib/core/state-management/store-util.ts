import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export function removeFromArray<T>(toRemove: T, array: T[]): T[] {
  const arrayCopy = [...array];
  arrayCopy.splice(array.indexOf(toRemove), 1);
  return arrayCopy;
}

export function updateInArray<T>(newValue: T, target: T, array: T[]): T[] {
  const index = array.indexOf(target);

  if (index < 0) {
    return array;
  }

  const arrayCopy = [...array];
  arrayCopy[index] = newValue;
  return arrayCopy;
}

export function addToArray<T>(toAdd: T, array: T[], numberOf = 1): T[] {
  if (toAdd) {
    return [...array, ...Array(numberOf).fill(toAdd)];
  }

  return [...array];
}

export function padArray(array: number[], padToSize: number, padWith: number): number[] {
  return new Array(padToSize)
    .fill(padWith)
    .map((fallback, index) => array[index] ? array[index] : fallback);
}

export function padArrayObservable(
  ids$: Observable<number[]>, padSize: number, padWith = 0
): Observable<number[]> {
  return ids$.pipe(
    map(ids => padArray(ids, padSize, padWith))
  );
}
