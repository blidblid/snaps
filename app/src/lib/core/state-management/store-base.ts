import { Observable, Subject } from 'rxjs';
import { scan, shareReplay, take, startWith, map, tap } from 'rxjs/operators';
import { ensureArray } from '../simple';

import { Action, StoreAction } from './store-model';
import { addToArray, removeFromArray, updateInArray } from './store-util';


export class StoreBase<T> {

  private entities$: Observable<T[]>;

  private actionSub: Subject<StoreAction<T>> = new Subject();

  constructor(
    private entityKey: string,
    private sortWith?: (entitites: T[]) => T[],
    private cap?: number,
    private preventDuplicates?: boolean,
    private mirrorToLocalStorage?: boolean
  ) {
    this.buildObservables();
    this.addAllFromLocalStorage();
  }

  get(): Observable<T[]> {
    return this.entities$;
  }

  add(entity: T): void {
    this.actionSub.next({ action: Action.Add, [this.entityKey]: entity });
  }

  update(entity: T, target: T): void {
    this.actionSub.next({ action: Action.Update, [this.entityKey]: entity, target });
  }

  clear(): void {
    this.actionSub.next({ action: Action.Clear });
  }

  set(entities: T | T[]): void {
    this.actionSub.next({ action: Action.Set, [this.entityKey]: entities });
  }

  push(): void {
    this.actionSub.next({ action: Action.Push });
  }

  pop(): void {
    this.actionSub.next({ action: Action.Pop });
  }

  remove(entity: T): void {
    this.actionSub.next({ action: Action.Remove, [this.entityKey]: entity });
  }

  private buildObservables(): void {
    this.entities$ = this.actionSub.pipe(
      scan<StoreAction<T>, T[]>((acc, curr) => {
        switch (curr.action) {
          case (Action.Set): {
            return ensureArray(curr[this.entityKey]);
          }

          case (Action.Add): {
            const capped = !!this.cap && (acc.length >= this.cap);
            const duplicate = this.preventDuplicates && acc.length > 0 && acc.some(a => a === curr[this.entityKey]);
            return (capped || duplicate)
              ? acc
              : addToArray(curr[this.entityKey], acc);
          }

          case (Action.Remove): {
            return removeFromArray(curr[this.entityKey], acc);
          }

          case (Action.Update): {
            return updateInArray(curr[this.entityKey], curr.target, acc);
          }

          case (Action.Push): {
            return [...acc];
          }

          case (Action.Pop): {
            return [...acc].slice(0, acc.length - 1);
          }

          case (Action.Clear): {
            return [];
          }
        }
      }, []),
      startWith([]),
      map(entities => this.sortWith ? this.sortWith(entities) : entities),
      tap(state => this.setLocalStorage(state)),
      shareReplay(1)
    );

    this.entities$.pipe(take(1)).subscribe();
  }

  private getLocalStorage(): T[] {
    return JSON.parse(localStorage.getItem(this.entityKey) || '[]');
  }

  private setLocalStorage(state: T[]): void {
    if (this.mirrorToLocalStorage) {
      localStorage.setItem(this.entityKey, JSON.stringify(state));
    }
  }

  private addAllFromLocalStorage(): void {
    if (this.mirrorToLocalStorage) {
      this.getLocalStorage().forEach(item => this.add(item));
    }
  }
}
