export const MOBILE_BREAKPOINT = '(max-width: 700px)';
export const SMALL_BREAKPOINT = '(max-width: 900px)';
export const LARGE_BREAKPOINT = '(max-width: 1470px)';
