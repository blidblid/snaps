import { NgModule } from '@angular/core';
import { MatExpansionPanelDefaultOptions } from '@angular/material/expansion';
import { MAT_TOOLTIP_DEFAULT_OPTIONS } from '@angular/material/tooltip';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';


export class MatExpansionPanelDefault implements MatExpansionPanelDefaultOptions {
  hideToggle = false;
  expandedHeight = '64px';
  collapsedHeight = '64px';
}

@NgModule({
  providers: [
    { provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: { appearance: 'outline', floatLabel: 'always' } },
    { provide: MAT_TOOLTIP_DEFAULT_OPTIONS, useValue: { showDelay: 250, hideDelay: 0, touchGestures: 'off' } }
  ]
})
export class MaterialServiceOverrideModule { }
