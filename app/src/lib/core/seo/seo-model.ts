export interface SeoTag {
  title?: string;
  name?: string;
  description?: string;
  image?: string;
  slug?: string;
}
