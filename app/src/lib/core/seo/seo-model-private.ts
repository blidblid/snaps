export const SEO_DEFAULT_TAGS = {
  title: 'snaps',
  name: 'snaps',
  description: 'Gaming communities',
  image: 'https://snaps.com/favicon.ico',
  slug: ''
};
