import { NgModule } from '@angular/core';
import { RouterModule, Route } from '@angular/router';

import { PageNotFoundComponent } from '@snaps/components';

import { BOOK_URL } from './main/main-model';
import { MainComponent } from './main/main.component';
import { BookComponent } from './routed/book/display/book.component';


const routes: Route[] = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: `${BOOK_URL}/:id`,
        component: BookComponent
      },
      {
        path: `${BOOK_URL}`,
        component: BookComponent
      }
    ]
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
