import { Component, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { SongStore } from 'lib/state/song-state';
import { Book, BookService } from '@snaps/api';

import { RoutesEntity } from '../../routes-entity';


@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  host: {
    'class': 'app-book'
  }
})
export class BookComponent extends RoutesEntity<Book> {

  songs$ = this.songStore.get();

  constructor(
    protected bookService: BookService,
    protected activatedRoute: ActivatedRoute,
    private songStore: SongStore
  ) {
    super(bookService, activatedRoute);
    this.songStore.get().subscribe(console.log);
  }
}

