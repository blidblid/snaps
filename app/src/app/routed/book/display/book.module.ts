import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaterialSharedModule } from '@snaps/core';
import { SongDisplayModule } from '@snaps/components';

import { BookComponent } from './book.component';


@NgModule({
  declarations: [
    BookComponent
  ],
  imports: [
    CommonModule,
    SongDisplayModule,
    MaterialSharedModule
  ]
})
export class BookModule { }
