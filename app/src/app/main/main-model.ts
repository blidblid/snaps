export const BOOK_URL = 'book';

export const TOP_NAV: SidenavCategory[] = [
  {
    name: 'Book',
    link: `/${BOOK_URL}`
  }
];

export interface SidenavCategory {
  name: string;
  link: string;
}
