import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { MaterialSharedModule } from '@snaps/core';
import { SongListModule } from '@snaps/components';

import { MainComponent } from './main.component';


@NgModule({
  imports: [
    CommonModule,
    MaterialSharedModule,
    SongListModule,
    RouterModule
  ],
  declarations: [
    MainComponent
  ],
  exports: [
    MainComponent
  ]
})
export class MainModule { }
