import {
  Component,
  ViewEncapsulation,
  ChangeDetectionStrategy,
  ViewChild,
  OnDestroy
} from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { Subject, Observable } from 'rxjs';
import { map, takeUntil, take, switchMap } from 'rxjs/operators';

import {
  BreakpointService,
  SMALL_BREAKPOINT,
  MOBILE_BREAKPOINT
} from '@snaps/core';
import { BookService, SongService } from '@snaps/api';

import { TOP_NAV } from './main-model';
import { Router } from '@angular/router';


@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  host: {
    '[class]': 'breakpointClass'
  }
})
export class MainComponent implements OnDestroy {

  topNav = TOP_NAV;
  breakpointClass: string;

  @ViewChild(MatSidenav, { static: false }) sidenav: MatSidenav;

  activatedView$: Observable<string>;

  breakpointClass$ = this.breakpoint.getMatches().pipe(
    map(breakpoint => {
      if (breakpoint.breakpoints[MOBILE_BREAKPOINT]) {
        return 'app-main-mobile app-main-small';
      } else if (breakpoint.breakpoints[SMALL_BREAKPOINT]) {
        return 'app-main-small';
      } else {
        return 'app-main-large';
      }
    })
  );

  songs$ = this.songService.getMany(['1', '5']);

  sidenavMode$ = this.breakpoint.getMatches().pipe(
    map(breakpoint => breakpoint.breakpoints[MOBILE_BREAKPOINT]),
    map(matches => matches ? 'over' : 'side')
  );

  sidenavOpen$ = this.sidenavMode$.pipe(take(1), map(mode => mode === 'side'));

  private createSub = new Subject<void>();
  private destroySub = new Subject<void>();

  constructor(
    private breakpoint: BreakpointService,
    private songService: SongService,
    private bookService: BookService,
    private router: Router
  ) {
    this.subscribe();
  }

  onCreate(): void {
    this.createSub.next();
  }

  toggleSidenav(): void {
    this.sidenav.toggle();
  }

  private subscribe(): void {
    this.breakpointClass$
      .pipe(takeUntil(this.destroySub))
      .subscribe(breakpointClass => this.breakpointClass = breakpointClass);

    const createdBookId$ = this.createSub.pipe(
      map(() => this.bookService.createEmptyBook())
    );

    createdBookId$
      .pipe(takeUntil(this.destroySub))
      .subscribe(id => this.router.navigateByUrl(`/book/${id}`));
  }

  ngOnDestroy(): void {
    this.destroySub.next();
    this.destroySub.complete();
  }
}
