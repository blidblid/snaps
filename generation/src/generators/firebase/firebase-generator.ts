import * as admin from 'firebase-admin';
import { readFileSync, readdirSync, writeFileSync, mkdir, mkdirSync, existsSync } from 'fs';
import { basename, join, parse } from 'path';
import { sync } from 'glob';
import { compileFromFile } from 'json-schema-to-typescript'


const serviceAccountPath = join(__dirname, 'snapsarn-firebase-adminsdk-tvr1m-a5e6520fb1.json');
const serviceAccount = JSON.parse(readFileSync(serviceAccountPath, 'utf8'));
const outputPath = join(__dirname, '..', '..', 'output');
const modelOutputPath = join(outputPath, 'model');

mkdirSync(modelOutputPath, { recursive: true });

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://snapsarn.firebaseio.com"',
  projectId: 'snapsarn',
  storageBucket: 'snapsarn.appspot.com'
});

export async function generate(): Promise<void> {
  for (const path of sync('../input/*', { cwd: __dirname }).map(p => join(__dirname, p))) {
    const name = basename(path);

    const model = await compileFromFile(join(path, 'schema.json'));
    writeFileSync(join(modelOutputPath, `${name}.ts`), model);

    const premadePath = join(path, 'premade');
    if (!existsSync(premadePath)) {
      continue;
    }

    for (const premadeItemPath of readdirSync(premadePath)) {
      await writeToFireStore(
        `crud/entity/${name}/${parse(premadeItemPath).name}`,
        join(premadePath, premadeItemPath)
      );
    }
  }
}

async function writeToFireStore(path: string, filePath: string): Promise<void> {
  const value = JSON.parse(readFileSync(filePath, 'utf8'));
  delete value['$schema'];

  const doc = admin.firestore().doc(path);
  const readDoc = await doc.get();

  if (readDoc.exists) {
    doc.update(value)
  } else {
    doc.set(value)
  }
}
