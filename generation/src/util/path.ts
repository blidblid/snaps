import { join } from 'path';
import { existsSync, mkdirSync } from 'fs';


const ASSETS = join(__dirname, '../../../app/src/assets');

const path = {
  gameImages: join(ASSETS, 'images', 'game')
};

Object.values(path).forEach(p => !existsSync(p) && mkdirSync(p, { recursive: true }));

export function getGameImagesPath(imageName: string): string {
  return join(path.gameImages, imageName);
}

export function imageExists(imageName: string): boolean {
  return existsSync(imageName);
}

export function stripExt(path: string): string {
  return path.replace(/\..*/g, '');
}
